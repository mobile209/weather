import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.lightBlueAccent,
        body: ListView(
          children:<Widget>[
            Container(
              height: 660,
              width: double.infinity,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage("https://png.pngtree.com/thumb_back/fh260/background/20220218/pngtree-hand-painted-cloudy-weather-background-image_961036.jpg"),
                  fit: BoxFit.cover
                )
              ),
              child: ListView(
                  children:<Widget>[
                Container(
                  margin: EdgeInsets.only(top: 50),
                  child: Center(
                    child: Text(
                      "Phang nga",
                      style: TextStyle(fontSize: 26),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 12),
                  child: Center(
                    child: Text(
                      "Cloudy",
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                ),
                Container(
                  child: Center(
                    child: Text(
                      "25°",
                      style: TextStyle(fontSize: 90),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
                Container(
                  height: 150,
                  margin: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white54,
                  ),
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Text(
                            "Today"
                        ),
                      ),
                      Divider(
                        color: Colors.grey,
                        indent: 20,
                        endIndent: 20,
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Text("Now"),
                                IconButton(
                                  icon: Icon(
                                    Icons.cloud_outlined,

                                  ),
                                  onPressed: (){},
                                ),
                                Text("25°")
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Text("7.00"),
                                IconButton(
                                  icon: Icon(
                                    Icons.wb_sunny_outlined,

                                  ),
                                  onPressed: (){},
                                ),
                                Text("26°")
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Text("8.00"),
                                IconButton(
                                  icon: Icon(
                                    Icons.cloudy_snowing
                                  ),
                                  onPressed: (){},
                                ),
                                Text("27°")
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Text("9.00"),
                                IconButton(
                                  icon: Icon(
                                    Icons.sunny_snowing,
                                  ),
                                  onPressed: (){},
                                ),
                                Text("28°")
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Text("10.00"),
                                IconButton(
                                  icon: Icon(
                                    Icons.ac_unit_outlined,

                                  ),
                                  onPressed: (){},
                                ),
                                Text("29°")
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Text("11.00"),
                                IconButton(
                                  icon: Icon(
                                    Icons.brightness_5_outlined,
                                  ),
                                  onPressed: (){},
                                ),
                                Text("30°")
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                    Container(
                      height: 400,
                      margin: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.black54
                      ),
                      child: Column(
                        children: <Widget>[
                          ListTile(
                            leading: Icon(Icons.cloud,color: Colors.white,),
                            title: Text("Tomorrow",style: TextStyle(color: Colors.white),),
                            trailing: Text("26°",style: TextStyle(fontSize: 26,color: Colors.white),),
                          ),
                          ListTile(
                            leading: Icon(Icons.sunny,color: Colors.white),
                            title: Text("Monday",style: TextStyle(color: Colors.white),),
                            trailing: Text("27°",style: TextStyle(fontSize: 26,color: Colors.white),),
                          ),
                          ListTile(
                            leading: Icon(Icons.cloudy_snowing,color: Colors.white),
                            title: Text("Tuesday",style: TextStyle(color: Colors.white),),
                            trailing: Text("28°",style: TextStyle(fontSize: 26,color: Colors.white),),
                          ),
                          ListTile(
                            leading: Icon(Icons.sunny_snowing,color: Colors.white),
                            title: Text("Wednesday",style: TextStyle(color: Colors.white),),
                            trailing: Text("27°",style: TextStyle(fontSize: 26,color: Colors.white),),
                          ),
                          ListTile(
                            leading: Icon(Icons.brightness_5,color: Colors.white),
                            title: Text("Thursday",style: TextStyle(color: Colors.white),),
                            trailing: Text("26°",style: TextStyle(fontSize: 26,color: Colors.white),),
                          ),
                          ListTile(
                            leading: Icon(Icons.cloud,color: Colors.white),
                            title: Text("Friday",style: TextStyle(color: Colors.white),),
                            trailing: Text("25°",style: TextStyle(fontSize: 26,color: Colors.white),),
                          ),
                          ListTile(
                            leading: Icon(Icons.sunny,color: Colors.white),
                            title: Text("Saturday",style: TextStyle(color: Colors.white),),
                            trailing: Text("26°",style: TextStyle(fontSize: 26,color: Colors.white),),
                          ),
                        ],
                      ),
                    )
              ]),
            )
          ],
        ),
      ),
    );
  }
}

